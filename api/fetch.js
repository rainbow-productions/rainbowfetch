import fetch from 'node-fetch';

export default async function handler(req, res) {

  // Get url parameter from request
  const url = req.query.url;

  try {
    // Use node-fetch to get contents
    const response = await fetch(url);
    const data = await response.text();
    
    // Return data in response
    res.status(200).send(data);

  } catch (error) {
    // Handle any errors
    console.error(error);
    res.status(500).send('Error fetching data');
  }

}
