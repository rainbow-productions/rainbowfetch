# RainbowFetch

This website fetches files from URLs by using the Fetch library. It is on my Gitlab as (rainbowfetch-lib)[https://gitlab.com/rainbow-productions/rainbowfetch-lib].


## Instalation & Selfhosting

### Installing the dependancies

To install the dependancies, use

`npm install`

If that doesn't work, use the following commands.

Command 1

`npm install fetch`

Command 2

`npm install express`

## Selfhosting

### Local Hosting

Run the following command to start up the program.

`npm start`

Then, go to (localhost:3000)[localhost:3000]

### Self-hosting platforms

Coming Soon

## How to use

Coming Soon