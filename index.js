const express = require('express');
const fetch = require('fetch');
const path = require('path');

const app = express();

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'));
});

// Set port
const port = 3000;



app.get('./api/fetch', async (req, res) => {
  const url = req.query.url;

  try {
    const response = await fetch(url);
    const fileText = await response.text();
    res.send(fileText);
  } catch (err) {
    console.error(err);
    res.status(500).send('Error fetching file'); 
  }
});


// Start server
app.listen(port, () => {
  console.log(`App listening on port ${port}`);
});
